(function () {

	var params = {
		min_number: 1,
		max_number: 10000,

		// We actually generate a list several times for more accurate timing measurements
		// the more is value the more accurate the measurements are
		attempts: 10,

		button_id: "generate_button",
		result_cont_id: "result_cont",
		stat_cont_id: "stat_cont"
	};

	var App = {
		init: function (params) {
			// Setting default values
			var params = params || {};
			params.min_number = params.min_number || 1;
			params.max_number = params.max_number || 10000;
			params.attempts = params.attempts || 10;
			params.button_id = params.button_id || "generate_button";
			params.result_cont_id = params.result_cont_id || "result_cont";
			params.stat_cont_id = params.stat_cont_id || "stat_cont";
			this.params = params;

			// An array we well be using
			this.init_arr = [];
			this.fin_arr = [];
			this.cacheElements();
			this.bindEvents();
		},
		cacheElements: function () {
			this.generate_button = document.getElementById( this.params.button_id );
			this.result_cont = document.getElementById( this.params.result_cont_id );
			this.stat_cont = document.getElementById( this.params.stat_cont_id );
		},
		bindEvents: function () {
			// Generate button click
			this.generate_button.addEventListener( "click", this.generateList );
		},
		generateList: function () {
			// Firstly, we fill an array with numbers such as [1...10000]
			var min = App.params.min_number;
			var max = App.params.max_number;
			var t0 = performance.now();
			App.init_arr = [];
			for (var i = min; i <= max; i++) {
				App.init_arr.push( i );
			}
			var t1 = performance.now();
			App.makeLogRecord( "Generating an array [" + min + "..." + max + "]", (t1 - t0).toFixed( 2 ) );

			// We use several methods but we need only one to display the result
			App.fin_arr = App.shuffleFisherYates( App.init_arr );

			// Now We test other algorithms to compare
			App.shuffleJavacriptSort( App.init_arr );
			App.fillAnswerCont();
		},
		makeLogRecord: function (text, time) {
			var cont = App.stat_cont;
			var element = document.createElement( 'p' );
			element.innerHTML = text + " took " + time + " milliseconds.";
			cont.appendChild( element );
		},
		fillAnswerCont: function () {
			var t0 = performance.now();
			var cont = App.result_cont;
			cont.innerHTML = '';
			var arr = App.init_arr;
			for (var i = 0; i < arr.length; i++) {
				var element = document.createElement( 'div' );
				element.innerHTML = arr[i];
				cont.appendChild( element );
			}
			var t1 = performance.now();
			App.makeLogRecord( "Rendering DOM elements", (t1 - t0).toFixed( 2 ) );
		},
		shuffleFisherYates: function (arr) { //  Fisher-Yates algorithm
			var attempts = App.params.attempts;
			var timings = [];

			for (var i = 1; i <= attempts; i++) {

				var array = arr;
				var t0 = performance.now();
				var currentIndex = array.length, temporaryValue, randomIndex;

				// While there remain elements to shuffle...
				while (0 !== currentIndex) {

					// Pick a remaining element...
					randomIndex = Math.floor( Math.random() * currentIndex );
					currentIndex -= 1;

					// And swap it with the current element.
					temporaryValue = array[currentIndex];
					array[currentIndex] = array[randomIndex];
					array[randomIndex] = temporaryValue;
				}
				var t1 = performance.now();
				timings.push( (t1 - t0).toFixed( 2 ) );
			}

			App.makeLogRecord( "Fisher-Yates algorithm", App.getAverage( timings ) );

			return array;
		},
		shuffleJavacriptSort: function (arr) {
			// Using Javascript sort function, actually does not really randomize
			// It is not effective as well, it makes more tahn 110,000 calls for array of 10000 numbers
			var attempts = App.params.attempts;
			var timings = [];
			for (var i = 1; i <= attempts; i++) {

				var array = arr;
				var t0 = performance.now();
				array.sort( function () {
					return .5 - Math.random();
				} );
				var t1 = performance.now();
				timings.push( (t1 - t0).toFixed( 2 ) );
			}
			App.makeLogRecord( "Javascript Sort algorithm", App.getAverage( timings ) );
			return array;
		},
		getAverage: function (arr) {
			var sum = 0;
			for (var i = 0; i < arr.length; i++) {
				sum += parseInt( arr[i], 10 );
			}
			return sum / arr.length;
		}
	};
	App.init( params );
})();