'use strict';

describe( 'Pandell web application', function () {

	beforeEach( function () {
		browser.ignoreSynchronization = true;
	} );

	// Checks if there is #generate_button element on the page
	it( "should display at least 'Generate' button ", function () {
		browser.get( '/index.html' );
		element.all( by.css( '#generate_button' ) ).then( function (items) {
			expect( items.length ).toBeGreaterThan( 0 );
		} )
	} );

	// Checks if button click event generates 10000 elements
	it( "should generate 10000 elements by clicking the button", function () {
		element.all( by.css( '#generate_button' ) ).click().then( function () {
			browser.sleep( 1000 ); // Give some time for generation
			element.all( by.css( '#result_cont div' ) ).then( function (items) {
				expect( items.length ).toBe( 1000 );
			} )
		} )
	} );

	// Checks if all the numbers are unique
	it( "should generate only unique numbers", function () {
		element.all( by.css( '#result_cont div' ) ).then( function (items) {
			var temp = [];
			items.forEach( function (item) {
				item.getText().then( function (text) {
					expect( temp.indexOf( text ) ).toBe( -1 );
					temp.push( text );
				} );
			} )
		} )
	} );
} );
