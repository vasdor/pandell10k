# README #

This app generates a list of 10,000 numbers in random order each time it is run.
Each number in the list is unique and is between 1 and 10,000 (inclusive).

There is no any prerequisitives to run the application, the only thing you have to do is to open index.html
in the browser.  


### How does it work? ###

After you hit the "Generate" button the application generates randomized list of numbers and displays on the page.
There is also some time measurements shown above the result.

The appplication also is designed to compare different algorithms of getting a randomized list. For now there are:

* Fisher-Yates algorithm - one of the most unbiased algorithm. It is the fastest of the two
* Javascript sort method - uses native sort function. Besides slowness it is criticized for it's randomizing 'features' 

### How do I get set up? ###

* The only software you need is a browser with Javascript turned on (which is default)
* To set up the application you can change some options ( params variable in the `js/script.js`):

Name        | Type   | default value  | Description 
------------|--------|----------------|-----------
min_number  |  number | 1             | The least number in the list
max_number  |  number | 10000         | The greatest number in the list
attempts    |  number | 10            | Shows how many times each algorithm runs. The more attempts it does the more accurate is performance assesment
button_id   |  string |generate_button| Id of element, which runs the generation
result_cont_id   |  string |result_cont| Id of element, where the list is supposed to be displayed
stat_cont_id   |  string |stat_cont| Id of element, where timing measurements are supposed to be displayed


### Running the tests ###

The tests are based on Javascript testing frameworks [Protractor](http://www.protractortest.org) and [Jasmine](http://jasmine.github.io/)
The configuration is in the `tests/protractor-conf.js` file and the tests are in the `tests/e2e/` directory
In this version we have following tests:

* Checks existance of some elements on the page
* Checks if list is generated and has exactly 10,000 elements in it
* Checks if all the numbers in the list are unique